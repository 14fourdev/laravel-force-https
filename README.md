# Force HTTPS for Laravel

## Installation

1. Require Laravel Force HTTPS with composer.

```
composer require 14four/laravel-force-https
```

1. Open your config/app.php and add the following to the providers array:

*note: Laravel > 5.5 skip this step*

```php
FourteenFour\ForceHttps\ForceHttpsServiceProvider::class,
```

1. Publish Config

```
php artisan vendor:publish --provider="FourteenFour\ForceHttps\ForceHttpsServiceProvider"
```

1. Add middleware to kernel.php

```php
\FourteenFour\ForceHttps\Middleware\ForceHttps::class,
```

*note: this should probably be added to the 'web' middleware*

## Config

Open the `config/forcehttps.php` configuration file to customize the config.

### Enable

`enable` will allow you to turn on and off the force HTTPS functionality of the middleware.

By default this uses the environment variable `FORCE_HTTPS`.

Use the following to disable the force HTTPS functionality.

```
FORCE_HTTPS=false
```

### Except

This will allow you to provide regex patterns that will be excluded from the force HTTPS functionality.

Example:
```php
$except = [
  '14four.com',
],
```

Keep in mind this will match the _fullUrl_ not just the domain. The example above will match `http://14four.com` and `http://example.com/post?14four.com`
