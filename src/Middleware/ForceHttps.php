<?php

namespace FourteenFour\ForceHttps\Middleware;

use Closure;

class ForceHttps
{

    /**
     * Handle requests in middleware to redirect to a secure route.
     *
     * @param Illuminate\Http\Request $request The request object for processing
     * @param Closure $next The next step in the middleware process
     * @return Closure,redirect
     */
    public function handle($request, Closure $next)
    {
        $url = $request->fullUrl();
        $ignored = $this->checkIfIgnored($url);

        if (config('forcehttps.enable') && !$ignored && !$request->secure()) {
            return redirect()->secure($request->getRequestUri());
        }

        return $next($request);
    }

    /**
     * Check if the URL is ignored in the configuration of the package
     *
     * @param string $url The requested URL
     * @return boolean
     */
    protected function checkIfIgnored($url)
    {
        $except = config('forcehttps.except');

        $ignorable = false;

        foreach($except as $ignore) {
            if (preg_match("/$ignore/i", $url)) {
                $ignorable = true;
            }
        }

        return $ignorable;
    }

}
