<?php
namespace FourteenFour\ForceHttps;

use Illuminate\Support\ServiceProvider;

/**
 * Force HTTPs service provider. Register package into service provider
 */
class ForceHttpsServiceProvider extends ServiceProvider
{

    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(__DIR__ . '/config/forcehttps.php', 'forcehttps');
    }

    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__ . '/config/forcehttps.php' => config_path('forcehttps.php'),
        ]);
    }

}
