<?php

return [


    /*
    |--------------------------------------------------------------------------
    | ENABLE FORCE HTTP
    |--------------------------------------------------------------------------
    |
    | Enables the the force HTTPS middleware. This can be used to disable the
    | middleware. Use the environment variable FORCE_HTTPS to configure.
    |
    */
    'enable' => env('FORCE_HTTPS', true),

    /*
    |--------------------------------------------------------------------------
    | SET EXCEPTION PATTERNS
    |--------------------------------------------------------------------------
    |
    | Set any regex patterns that you want to exclude from the force http
    | urls. Warning this checks the Full URL so query params are excluded
    | as well as domains (eg '14four.com' would match www.14four.com and
    | example.com/post?14four.com)
    |
    */
    'except' => [],

];
