<?php namespace Tests\Unit;

use Tests\BaseTestCase;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Artisan;

class ServiceProviderRegistrationTest extends BaseTestCase
{

    public function test_enabled_set()
    {
        $this->assertNotEmpty(config('forcehttps'));

        $this->assertEquals(config('forcehttps.enable'), true);
    }

    public function test_publish()
    {
        $file = config_path() . '/forcehttps.php';

        $this->assertFalse(file_exists($file));

        Artisan::call('vendor:publish', [
            '--provider' => 'FourteenFour\ForceHttps\ForceHttpsServiceProvider',
        ]);

        $this->assertTrue(file_exists($file));
    }

}
