<?php namespace Tests\Unit;

use Illuminate\Http\Request;
use FourteenFour\ForceHttps\Middleware\ForceHttps;
use Tests\BaseTestCase;

class ForceHttpsMiddlewareTest extends BaseTestCase {

    public function test_redirect_route()
    {
        $request = Request::create('/', 'GET');

        $middleware = new ForceHttps;

        $response = $middleware->handle($request, function () {
        });

        $this->assertEquals($response->getStatusCode(), 302);
    }

}
